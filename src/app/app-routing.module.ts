import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserLoginTempComponent } from './shared/user-login-temp/user-login-temp.component';

const routes: Routes = [
  { path: '', component: UserLoginTempComponent },
  {
    path: 'chat',
    loadChildren: () => import('./chat/chat.module').then((m) => m.ChatModule),
  },
  {
    path: 'groups',
    loadChildren: () =>
      import('./groups/groups.module').then((m) => m.GroupsModule),
  },
  {
    path: 'asks',
    loadChildren: () => import('./asks/asks.module').then((m) => m.AsksModule),
  },
  { path: '**', redirectTo: 'groups', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
