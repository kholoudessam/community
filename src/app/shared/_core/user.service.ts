import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private authUrl = environment.baseUrl + '/user';
  isLoading$ = new BehaviorSubject<boolean>(false);

  constructor(private _http: HttpClient, private router: Router) {}

  getUser(id: number) {
    this._http.get('').subscribe((user) => {
      localStorage.setItem('user', JSON.stringify(user));
    });
  }

  login(email: string, password: string) {
    this.isLoading$.next(true);

    this._http
      .post<LoginSuccess>(`${this.authUrl}/login`, {
        email,
        password,
      })
      .subscribe(
        (res) => {
          localStorage.setItem('userData', JSON.stringify(res.user));
          localStorage.setItem('token', res.accessToken);
          this.isLoading$.next(false);
          this.router.navigateByUrl('/asks');
        },
        (err) => {
          alert('fail login');
          this.isLoading$.next(false);
        }
      );
  }

  getUserObj(): UserData {
    return JSON.parse(localStorage.getItem('userData') || '{}');
  }
  getUserId() {
    const userObj = this.getUserObj();

    return userObj.id ? userObj.id : 0;
  }
}

enum UserTypes {
  'COMPANY',
  'USER',
}

export interface UserData {
  cover: string;
  createdAt: string;
  email: string;
  id: number;
  image: string;
  isFlowing: boolean;
  mobile: string;
  password: string;
  rateCount: number;
  rateLength: number;
  salt: string;
  type: UserTypes;
  updatedAt: string;
  username: string;
  verificationCode: string;
  verificationExpire: number;
  verified: boolean;
}

export interface LoginSuccess {
  accessToken: string;
  user: UserData;
}
