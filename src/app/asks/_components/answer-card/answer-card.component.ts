import { Component, Input, OnInit } from '@angular/core';
import { Comment } from '../../_core/models/comment';

@Component({
  selector: 'app-answer-card',
  templateUrl: './answer-card.component.html',
  styleUrls: ['./answer-card.component.scss'],
})
export class AnswerCardComponent implements OnInit {
  @Input() comment!: Comment;
  constructor() {}

  ngOnInit(): void {}
}
