export const environment = {
  production: true,
  baseUrl: 'https://backend.sarh.ae',
  firebaseConfig: {
    apiKey: 'AIzaSyBkeFJylT92fqKtE20-cV7ZJc7xoFjg4us',
    authDomain: 'sarh-app-1590062112076.firebaseapp.com',
    projectId: 'sarh-app-1590062112076',
    storageBucket: 'sarh-app-1590062112076.appspot.com',
    messagingSenderId: '630136425508',
    appId: '1:630136425508:web:6fa6c4cdcc0182304e5cb1',
    measurementId: '${config.measurementId}',
  },
};
