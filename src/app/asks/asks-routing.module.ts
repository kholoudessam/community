import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewQuestionComponent } from './_components/new-question/new-question.component';
import { QuestionDetailsComponent } from './_components/question-details/question-details.component';
import { QuestionsListComponent } from './_components/questions-list/questions-list.component';

const routes: Routes = [
  { path: '', component: QuestionsListComponent },
  // { path: 'new-question', component: NewQuestionComponent },
  { path: ':id', component: QuestionDetailsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AsksRoutingModule {}
