import { Timestamp } from '@angular/fire/firestore';

export interface Question {
  commentCount?: number;
  content?: string;
  createdAt?: Timestamp;
  createdAtFormatted?: string;
  userId?: number;
  userImage?: string;
  userName?: string;
  viewCount?: number;
  qId?: string;
}

interface TimestampFb {
  seconds: string;
  nanoseconds: string;
}
