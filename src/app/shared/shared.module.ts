import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import { UserLoginTempComponent } from './user-login-temp/user-login-temp.component';

const modules = [
  MatCardModule,
  MatButtonModule,
  MatInputModule,
  MatDialogModule,
  FormsModule,
];

@NgModule({
  declarations: [UserLoginTempComponent],
  imports: [CommonModule, ...modules],
  exports: [...modules, UserLoginTempComponent],
})
export class SharedModule {}
