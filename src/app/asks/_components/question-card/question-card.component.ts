import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from '../../_core/models/question';

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.scss'],
})
export class QuestionCardComponent {
  @Input() question!: Question;
  constructor(private router: Router) {}

  goToQuestionDetails(id: string) {
    console.log('id', id);
    if (!id) return;

    this.router.navigate(['/asks', id]);
  }
}
