import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
} from '@angular/fire/compat/firestore';
import * as moment from 'moment';
import { BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { UserService } from 'src/app/shared/_core/user.service';

import { Question } from '../models/question';
import { Comment } from '../models/comment';

@Injectable({
  providedIn: 'root',
})
export class QuestionsService {
  private QuestionsCollection: AngularFirestoreCollection<Question>;
  isQuestions = new BehaviorSubject<boolean>(true);
  isComments = new BehaviorSubject<boolean>(true);

  constructor(private afs: AngularFirestore, private userService: UserService) {
    this.QuestionsCollection = afs.collection<Question>('questions');
  }

  getQuestionsByUser() {
    return this.afs
      .collection<Question>('questions', (ref) =>
        ref
          .where('userId', '==', this.userService.getUserId())
          .orderBy('createdAt', 'desc')
      )
      .valueChanges({ idField: 'qId' })
      .pipe(
        tap((qs) => console.log('here', qs)),
        tap((qs) => {
          if (qs.length >= 1) this.isQuestions.next(true);
          else this.isQuestions.next(false);
        }),
        map((qs) =>
          qs.map(
            (q: Question) =>
              ({
                ...q,
                createdAtFormatted: moment(q.createdAt?.toDate()).fromNow(),
              } as Question)
          )
        )
      );
  }

  getQuestion(qsId: string) {
    return this.QuestionsCollection.doc(qsId).valueChanges();
  }

  getQuestionComments(qsId: string) {
    return this.QuestionsCollection.doc(qsId)
      .collection<Comment>('comments', (ref) =>
        ref.orderBy('createdAt', 'desc')
      )
      .valueChanges()
      .pipe(
        tap((c) => {
          if (c.length >= 1) this.isComments.next(true);
          else this.isComments.next(false);
        }),
        map((c) =>
          c.map(
            (c: Comment) =>
              ({
                ...c,
                createdAtFormatted: moment(c.createdAt?.toDate()).fromNow(),
              } as Comment)
          )
        )
      );
  }

  addQuestion(question: Question) {
    this.QuestionsCollection.add(question);
  }

  addComment(comment: Comment, qsId: string) {
    this.QuestionsCollection.doc(qsId).collection('comments').add(comment);
  }
}
