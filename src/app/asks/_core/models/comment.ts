import { Timestamp } from 'firebase/firestore';

export interface Comment {
  comment?: string;
  createdAt?: Timestamp;
  createdAtFormatted?: string;
  likeCount?: number;
  userId?: number;
  userImage?: string;
  userName?: string;
  likeUsers?: number[];
}
