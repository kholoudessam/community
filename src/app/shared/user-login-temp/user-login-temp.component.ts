import { Component, OnInit } from '@angular/core';
import { UserService } from '../_core/user.service';

@Component({
  selector: 'app-user-login-temp',
  templateUrl: './user-login-temp.component.html',
  styleUrls: ['./user-login-temp.component.scss'],
})
export class UserLoginTempComponent implements OnInit {
  constructor(private authService: UserService) {}

  loading$ = this.authService.isLoading$;

  ngOnInit(): void {}

  email = '';
  password = '';

  login() {
    this.authService.login(this.email, this.password);
  }
}
