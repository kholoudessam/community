import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { Observable } from 'rxjs';
import { Question } from '../../_core/models/question';
import { Comment } from '../../_core/models/comment';
import { QuestionsService } from '../../_core/services/questions.service';
import { MatDialog } from '@angular/material/dialog';
import { QuestionAnswerFormComponent } from '../question-answer-form/question-answer-form.component';
import { UserService } from 'src/app/shared/_core/user.service';
import { Timestamp } from 'firebase/firestore';
import { environment } from 'src/environments/environment';

@UntilDestroy()
@Component({
  selector: 'app-question-details',
  templateUrl: './question-details.component.html',
  styleUrls: ['./question-details.component.scss'],
})
export class QuestionDetailsComponent implements OnInit {
  question$!: Observable<Question | undefined>;
  comments$!: Observable<Comment[]>;
  isComments$: Observable<boolean> = this.questionsService.isComments;
  id = '';
  constructor(
    private route: ActivatedRoute,
    private questionsService: QuestionsService,
    private dialog: MatDialog,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.route.params.pipe(untilDestroyed(this)).subscribe((params) => {
      this.id = params['id'];

      this.question$ = this.questionsService.getQuestion(this.id);

      this.comments$ = this.questionsService.getQuestionComments(this.id);

      this.questionsService
        .getQuestionComments(this.id)
        .subscribe((d) => console.log(d));
    });
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(QuestionAnswerFormComponent, {
      width: '400px',
      data: '',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');

      if (!result || result?.trim() == '') return;
      const userId = this.userService.getUserId();
      if (!userId) return;
      const { username, image } = this.userService.getUserObj();
      const comment: Comment = {
        comment: result,
        createdAt: Timestamp.now(),
        userId,
        userImage: environment.baseUrl + '/' + image,
        userName: username,
      };

      console.log(comment);
      this.questionsService.addComment(comment, this.id);
    });
  }
}
