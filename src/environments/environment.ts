// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl: 'https://backend.sarh.ae',
  firebaseConfig: {
    apiKey: 'AIzaSyBkeFJylT92fqKtE20-cV7ZJc7xoFjg4us',
    authDomain: 'sarh-app-1590062112076.firebaseapp.com',
    projectId: 'sarh-app-1590062112076',
    storageBucket: 'sarh-app-1590062112076.appspot.com',
    messagingSenderId: '630136425508',
    appId: '1:630136425508:web:6fa6c4cdcc0182304e5cb1',
    measurementId: '${config.measurementId}',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
