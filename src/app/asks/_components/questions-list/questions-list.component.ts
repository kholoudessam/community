import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Question } from '../../_core/models/question';
import { QuestionsService } from '../../_core/services/questions.service';
import { MatDialog } from '@angular/material/dialog';
import { Timestamp } from '@angular/fire/firestore';
import { NewQuestionComponent } from '../new-question/new-question.component';
import { UserService } from 'src/app/shared/_core/user.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-questions-list',
  templateUrl: './questions-list.component.html',
  styleUrls: ['./questions-list.component.scss'],
})
export class QuestionsListComponent implements OnInit {
  questions$: Observable<Question[]> =
    this.questionsService.getQuestionsByUser();
  isQuest$: Observable<boolean> = this.questionsService.isQuestions;
  searchKey = '';
  constructor(
    private questionsService: QuestionsService,
    private userService: UserService,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {}

  searchQuestions() {}

  openDialog(): void {
    const dialogRef = this.dialog.open(NewQuestionComponent, {
      width: '400px',
      data: '',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');

      if (!result || result?.trim() == '') return;
      const userId = this.userService.getUserId();
      if (!userId) return;
      const { username, image } = this.userService.getUserObj();
      const quest: Question = {
        content: result,
        createdAt: Timestamp.now(),
        userId,
        userImage: environment.baseUrl + '/' + image,
        userName: username,
        commentCount: 0,
        viewCount: 0,
      };

      console.log(quest);
      this.questionsService.addQuestion(quest);
    });
  }
}
