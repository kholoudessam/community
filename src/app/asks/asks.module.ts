import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionsListComponent } from './_components/questions-list/questions-list.component';
import { AsksRoutingModule } from './asks-routing.module';
import { NewQuestionComponent } from './_components/new-question/new-question.component';
import { QuestionAnswerFormComponent } from './_components/question-answer-form/question-answer-form.component';
import { QuestionDetailsComponent } from './_components/question-details/question-details.component';
import { SharedModule } from '../shared/shared.module';
import { QuestionCardComponent } from './_components/question-card/question-card.component';
import { AnswerCardComponent } from './_components/answer-card/answer-card.component';

@NgModule({
  declarations: [
    QuestionsListComponent,
    NewQuestionComponent,
    QuestionAnswerFormComponent,
    QuestionDetailsComponent,
    QuestionCardComponent,
    AnswerCardComponent,
  ],
  imports: [CommonModule, AsksRoutingModule, SharedModule],
})
export class AsksModule {}
